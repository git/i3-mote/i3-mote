/*
 *  ======== empty_min.c ========
 */
/* XDCtools Header files */
#include <xdc/std.h>
#include <xdc/runtime/System.h>
#include <xdc/runtime/Error.h>

/* BIOS Header files */
#include <ti/sysbios/BIOS.h>
#include <ti/sysbios/knl/Task.h>
#include <ti/sysbios/knl/Clock.h>
#include <ti/sysbios/knl/Semaphore.h>
#include <ti/sysbios/knl/Event.h>

/* TI-RTOS Header files */
#include <ti/drivers/GPIO.h>
#include <ti/drivers/I2C.h>
#include <ti/drivers/PIN.h>
 #include <ti/drivers/SPI.h>
#include <ti/drivers/UART.h>
// #include <ti/drivers/Watchdog.h>

/* Board Header files */
#include "Board.h"

#include "uart_printf.h"

#define TASKSTACKSIZE   (1024)

Task_Struct task0Struct;
Char task0Stack[TASKSTACKSIZE];

Task_Struct task1Struct;
Char task1Stack[TASKSTACKSIZE];

Task_Struct task2Struct;
Char task2Stack[TASKSTACKSIZE];

#define OPT3001
#define HDC1080
#define BMP280
#define TMP007

#undef MPU9250
#undef LISHH12


/* Semaphore */
Semaphore_Struct semStruct;
Semaphore_Handle semHandle;


Event_Handle myEvent;
Error_Block eb;



void buttonCallbackFxn(unsigned int pinId) {

 	Semaphore_post(semHandle);

}


Void uartFxn(UArg arg0, UArg arg1)
{
	char input;

	while (1) {
		// Blocking Read
		input=uart_getch();
		if(input=='g')
			Semaphore_post(semHandle);

	    Task_sleep((UInt)arg0);
	}

}

const uint8_t m25p40_id[5]={0x00,0x20,0x71,0x15,0x10};

Void taskFxn(UArg arg0, UArg arg1)
{

	int i;

	uint8_t         rxBufferPointer[4];
    uint8_t         txBufferPointer[4];

    SPI_Handle      spi;
    SPI_Params      spiParams;
    SPI_Transaction spiTransaction;

    SPI_Params_init(&spiParams);
    // Slave mode
    spiParams.mode = SPI_MASTER;
    spiParams.bitRate = 500000;

    spiParams.frameFormat = SPI_POL1_PHA1;

	int flagerr=0;

    spi=SPI_open(Board_SPI2,&spiParams);
    if(!spi){
      System_printf("SPI did not open");
    }
    System_printf("SPI Init Done\r\n");

    GPIO_write(Board_LED0, 1);
    GPIO_write(Board_LED1, 1);

    System_printf("Press button:\r\n");
	Semaphore_pend(semHandle, BIOS_WAIT_FOREVER);

	/* Start test */
    GPIO_write(Board_LED0, 0);
    GPIO_write(Board_LED1, 0);

    GPIO_write(Board_SPI2_CS, 0);

    /* M25P40 Read Id Command */
	txBufferPointer[0]=0x9E;

	spiTransaction.rxBuf = rxBufferPointer;
	spiTransaction.txBuf = txBufferPointer;
	spiTransaction.count = 5;

	/* Write Transaction */
	if (SPI_transfer(spi,&spiTransaction)) {
		System_printf("M25P40-RegID:");
		for(i=1;i<5;i++){
			System_printf(" %x",rxBufferPointer[i]);
			if(m25p40_id[i]!=rxBufferPointer[i]){
				Event_post(myEvent, Event_Id_00);
				flagerr=1;
			}
		}
		System_printf("\r\n");
	}
	else{
		System_printf("SPI Transaction Failed\r\n");
		Event_post(myEvent, Event_Id_00);
		flagerr=1;
	}
	Task_sleep(10);

    GPIO_write(Board_SPI2_CS, 1);

	/* OK! */
	if(flagerr==0){
		Event_post(myEvent, Event_Id_01);
		System_printf("***\r\n");
		System_flush();
	}else{
		System_printf("KO!\r\n");
		System_flush();
	}

	Task_sleep((UInt)arg0);

    /* Deinitialized I2C */
    SPI_close(spi);
}

/*
 *  ======== heartBeatFxn ========
 *  Toggle the Board_LED0. The Task_sleep is determined by arg0 which
 *  is configured for the heartBeat Task instance.
 */
Void heartBeatFxn(UArg arg0, UArg arg1)
{

	UInt events;

	events=Event_pend(myEvent,Event_Id_NONE,Event_Id_00+Event_Id_01,BIOS_WAIT_FOREVER);

	if(events & Event_Id_00){
	  while (1)
	  {
        Task_sleep((UInt)arg0);
        GPIO_toggle(Board_LED0);
      }
	}

	if(events & Event_Id_01){
      while (1)
      {
        Task_sleep((UInt)arg0);
        GPIO_toggle(Board_LED1);
      }
	}
}



/*
 *  ======== main ========
 */


int main(void)
{
    Task_Params taskParams;

    Semaphore_Params semParams;


    /* Call board init functions */
    Board_initGPIO();
    Board_initGeneral();
    //Board_initI2C();
    Board_initSPI();
    Board_initUART();
    // Board_initWatchdog();

    /* Default instance configuration params */
    Error_init(&eb);
    myEvent = Event_create(NULL,&eb);
    if (myEvent == NULL) {
        System_abort("Event create failed");
    }


    UART_Params uartParams;
    UART_Params_init(&uartParams);
    uartParams.baudRate = 115200;
    uartParams.readEcho = UART_ECHO_OFF;
    UartPrintf_init(UART_open(Board_UART, &uartParams));

    /* Construct Test Task thread */
    Task_Params_init(&taskParams);
    taskParams.arg0 = 100000 / Clock_tickPeriod;
    taskParams.stackSize = TASKSTACKSIZE;
    taskParams.stack = &task1Stack;
    Task_construct(&task1Struct, (Task_FuncPtr)taskFxn, &taskParams, NULL);


    /* Construct heartBeat Task  thread */
    Task_Params_init(&taskParams);
    taskParams.arg0 = 100000 / Clock_tickPeriod;
    taskParams.stackSize = TASKSTACKSIZE;
    taskParams.stack = &task0Stack;
    Task_construct(&task0Struct, (Task_FuncPtr)heartBeatFxn, &taskParams, NULL);

    /* Construct Uart Task  thread */
    Task_Params_init(&taskParams);
    taskParams.arg0 = 100000 / Clock_tickPeriod;
    taskParams.stackSize = TASKSTACKSIZE;
    taskParams.stack = &task2Stack;
    Task_construct(&task2Struct, (Task_FuncPtr)uartFxn, &taskParams, NULL);


    /* Construct Semaphore and Obtain instance handle */
    Semaphore_Params_init(&semParams);
    Semaphore_construct(&semStruct,0, &semParams);
    semHandle = Semaphore_handle(&semStruct);


    /* Setup callback for button pins */
    GPIO_setCallback(Board_BUTTON0,buttonCallbackFxn);
    GPIO_enableInt(Board_BUTTON0);

    /* Setup callback for button pins */
    GPIO_write(Board_LED0, 0);
    GPIO_write(Board_LED1, 0);
    GPIO_write(Board_SPI1_CS, 1);

    /* Start BIOS */
    BIOS_start();

    return (0);
}
