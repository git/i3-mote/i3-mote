Basic Test Package for I3MOTE-MSP432
====================================

Test MSP432-Blink-SysTick
-------------------------
Blinks two LEDs using SysTick (which is sourced from MCLK). BT1 pauses blinking.

#### Configuration:
* DCO @ 12MH
* MCLK sourced from DCO.

***

Test MSP432-ClockSystem
-------------------------
Configures MCLK, HSMCLK and ACLK outputs for crystal testing. 

#### Configuration:
* P4.2 -> ACLK:  32.768 KHz (Source 32.768Khz crytal @ LFXTL)
* P4.3 -> MCLK:  12.000 MHz (Source 48Mhz crytal @ HFXTL)
* P4.4 -> HSMCLK: 6.000 MHz
* P7.0 -> SMCLK:  6.000 Mhz

***

Test MSP432-DebugUART-EchoPC
----------------------------
Echoes back characters received via a PC serial port. 

#### Configuration:
* DCO @ 12MHz -> SMCLK 
* SMCLK sourced from DCO.
* Uart 115200.

***

Test MSP432-I2C-M24xx256
------------------------
Writes and checks on-board I2C EEPROM.

#### Configuration:
* DCO @ 12MH 
* SMCLK sourced from DCO.
* I2C 100kbps.

***

Test MSP432-I2C-Sensors
-----------------------
Reads General Purpose Sensors IDs.

#### Configuration:
* DCO @ 12MH 
* SMCLK sourced from DCO.
* I2C 100kbps.

***

Test MSP432-3wSPI-M25P40
------------------------
Reads on-board SPI flash memory ID (M25P40).

#### Configuration:
* DCO @ 12MH 
* SMCLK sourced from DCO.
* SPI 100kbps.

***

Test MSP432-3wSPI-SlaveIRQ-CC2650-Master
----------------------------------------
SPI slave echoes to SPI master using 3-wire mode.
Incrementing data is sent by the master starting at 0x01. 
Received data is expected to be same as the previous transmission.  
Transactions are started from the slave (MSP432) using the IRQ line.

#### Configuration:
* DCO @ 12MH 
* SMCLK sourced from DCO.
* Requires Test CC2650-3wSPI-Master-MSP432-SlaveIRQ running on CC2650

***

Tested using *MSPWare_3_30_00_18* and *TI-v15.12.1-LTS* Compiler



