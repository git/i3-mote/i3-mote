/*
 * Copyright (c) 2015-2016, Texas Instruments Incorporated
 * All rights reserved.
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions
 * are met:
 *
 * *  Redistributions of source code must retain the above copyright
 *    notice, this list of conditions and the following disclaimer.
 *
 * *  Redistributions in binary form must reproduce the above copyright
 *    notice, this list of conditions and the following disclaimer in the
 *    documentation and/or other materials provided with the distribution.
 *
 * *  Neither the name of Texas Instruments Incorporated nor the names of
 *    its contributors may be used to endorse or promote products derived
 *    from this software without specific prior written permission.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
 * AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO,
 * THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR
 * PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT OWNER OR
 * CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL,
 * EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO,
 * PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS;
 * OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY,
 * WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR
 * OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE,
 * EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 */

#ifndef __BOARD_H
#define __BOARD_H

#ifdef __cplusplus
extern "C" {
#endif

#include "MSP432P401R_I3M.h"

#define Board_initADC               MSP_I3MSP432P401R_initADC
#define Board_initGeneral           MSP_I3MSP432P401R_initGeneral
#define Board_initGPIO              MSP_I3MSP432P401R_initGPIO
#define Board_initI2C               MSP_I3MSP432P401R_initI2C
#define Board_initPWM               MSP_I3MSP432P401R_initPWM
#define Board_initSDSPI             MSP_I3MSP432P401R_initSDSPI
#define Board_initSPI               MSP_I3MSP432P401R_initSPI
#define Board_initUART              MSP_I3MSP432P401R_initUART
#define Board_initWatchdog          MSP_I3MSP432P401R_initWatchdog
#define Board_initWiFi              MSP_I3MSP432P401R_initWiFi

#define Board_ADC0                  MSP_I3MSP432P401R_ADC0
#define Board_ADC1                  MSP_I3MSP432P401R_ADC1

#define Board_LED_ON                MSP_I3MSP432P401R_LED_ON
#define Board_LED_OFF               MSP_I3MSP432P401R_LED_OFF

#define Board_BUTTON0               MSP_I3MSP432P401R_S1
#define Board_BUTTON1               MSP_I3MSP432P401R_S2
#define Board_LED0                  MSP_I3MSP432P401R_LED_GREEN
#define Board_LED1                  MSP_I3MSP432P401R_LED_RED


#define Board_SPI0_CS               MSP_I3MSP432P401R_SPIB0_CS
#define Board_SPI1_CS               MSP_I3MSP432P401R_SPIA1_CS
#define Board_SPI2_CS               MSP_I3MSP432P401R_SPIA2_CS

#define Board_SPI0_IRQ              MSP_I3MSP432P401R_SPIB0_IRQ
#define Board_SPI1_IRQ              MSP_I3MSP432P401R_SPIA1_IRQ
#define Board_SPI2_IRQ              MSP_I3MSP432P401R_SPIA2_IRQ



/*
 * MSP_I3MSP432P401R_LED_GREEN & MSP_I3MSP432P401R_LED_BLUE are used for
 * PWM examples.  Uncomment the following lines if you would like to control
 * the LEDs with the GPIO driver.
 */


#define Board_I2C0                  MSP_I3MSP432P401R_I2CB2

//#define Board_PWM0                  MSP_I3MSP432P401R_PWM_TA1_1
//#define Board_PWM1                  MSP_I3MSP432P401R_PWM_TA1_2

//#define Board_SDSPI0                MSP_I3MSP432P401R_SDSPIB0

#define Board_SPI0                  MSP_I3MSP432P401R_SPIB0
#define Board_SPI1                  MSP_I3MSP432P401R_SPIA1
#define Board_SPI2                  MSP_I3MSP432P401R_SPIA2

#define Board_UART                  MSP_I3MSP432P401R_UARTA0
//#define Board_UART0                 MSP_I3MSP432P401R_UARTA0
//#define Board_UART1                 MSP_I3MSP432P401R_UARTA2

#define Board_WATCHDOG0             MSP_I3MSP432P401R_WATCHDOG

//#define Board_WIFI                  MSP_I3MSP432P401R_WIFI
//#define Board_WIFI_SPI              MSP_I3MSP432P401R_SPIB0

/* Board specific I2C addresses */

#define EEPROM_SLAVE_ADDRESS (0x50)

/* Sensors */
#define Board_INA219_ADDR    (0x41)
#define Board_HDC1000_ADDR   (0x43)
#define Board_HDC1080_ADDR   (0x40)
#define Board_TMP007_ADDR    (0x44)
#define Board_OPT3001_ADDR   (0x45)
#define Board_MPU9250_ADDR   (0x68)
#define Board_BMP280_ADDR    (0x77)
#define Board_LISHH12_ADDR   (0x1E)


#ifdef __cplusplus
}
#endif

#endif /* __BOARD_H */




