/*
 *  ======== empty_min.c ========
 */
/* XDCtools Header files */
#include <xdc/std.h>
#include <xdc/runtime/System.h>
#include <xdc/runtime/Error.h>

/* BIOS Header files */
#include <ti/sysbios/BIOS.h>
#include <ti/sysbios/knl/Task.h>
#include <ti/sysbios/knl/Clock.h>
#include <ti/sysbios/knl/Semaphore.h>
#include <ti/sysbios/knl/Event.h>

/* TI-RTOS Header files */
#include <ti/drivers/GPIO.h>
#include <ti/drivers/I2C.h>
#include <ti/drivers/PIN.h>
// #include <ti/drivers/SPI.h>
#include <ti/drivers/UART.h>
// #include <ti/drivers/Watchdog.h>

/* Board Header files */
#include "Board.h"

#include "uart_printf.h"

#define TASKSTACKSIZE   (1024)

Task_Struct task0Struct;
Char task0Stack[TASKSTACKSIZE];

Task_Struct task1Struct;
Char task1Stack[TASKSTACKSIZE];

Task_Struct task2Struct;
Char task2Stack[TASKSTACKSIZE];

#define OPT3001
#define HDC1080
#define BMP280
#define TMP007

#undef MPU9250
#undef LISHH12


/* Semaphore */
Semaphore_Struct semStruct;
Semaphore_Handle semHandle;


Event_Handle myEvent;
Error_Block eb;


/*
 * Application LED pin configuration table:
 *   - All LEDs board LEDs are off.
 */
PIN_Config ledPinTable[] = {
    Board_LED0 | PIN_GPIO_OUTPUT_EN | PIN_GPIO_LOW | PIN_PUSHPULL | PIN_DRVSTR_MAX,
    Board_LED1 | PIN_GPIO_OUTPUT_EN | PIN_GPIO_LOW | PIN_PUSHPULL | PIN_DRVSTR_MAX,
    PIN_TERMINATE
};

PIN_Config buttonPinTable[] = {
    Board_BUTTON0  | PIN_INPUT_EN | PIN_PULLUP | PIN_IRQ_NEGEDGE,
    //Board_BUTTON1  | PIN_INPUT_EN | PIN_PULLUP | PIN_IRQ_NEGEDGE,
    PIN_TERMINATE
};


void buttonCallbackFxn(unsigned int pinId) {

    GPIO_write(Board_LED0, 0);
    GPIO_write(Board_LED1, 0);
    //CPUdelay(8000*200);
    GPIO_write(Board_LED0, 1);
    GPIO_write(Board_LED1, 1);

	//Event_post(myEvent,Event_Id_02);
	Semaphore_post(semHandle);

}


Void uartFxn(UArg arg0, UArg arg1)
{
	char input;

	while (1) {
		// Blocking Read
		input=uart_getch();
		if(input=='g')
			Semaphore_post(semHandle);
		    //Event_post(myEvent,Event_Id_02);

	    Task_sleep((UInt)arg0);
	}

}

#define NTEST 32

Void taskFxn(UArg arg0, UArg arg1)
{

	uint8_t         iter=0;

	uint8_t         txBuffer[1];
    uint8_t         rxBuffer[2];
    I2C_Handle      i2c;
    I2C_Params      i2cParams;
    I2C_Transaction i2cTransaction;

	int flagerr=0;

    /* Create I2C for usage */
    I2C_Params_init(&i2cParams);
    i2cParams.bitRate = I2C_100kHz;
    i2c = I2C_open(Board_I2C0, &i2cParams);
    if (i2c == NULL) {
        System_abort("Error Initializing I2C\r\n");
    }

    System_printf("I2C Init Done\r\n");

    GPIO_write(Board_LED0, 1);
    GPIO_write(Board_LED1, 1);

    System_printf("Press button:\r\n");
	Semaphore_pend(semHandle, BIOS_WAIT_FOREVER);

	//events=Event_pend(myEvent,Event_Id_NONE,Event_Id_02,BIOS_WAIT_FOREVER);
	//if(!(events&Event_Id_02)){
	//    System_abort("Unhandled event\r\n");
	//}

	/* Start test */
    GPIO_write(Board_LED0, 0);
    GPIO_write(Board_LED1, 0);

	uint16_t addr;
	uint8_t txdata;
	uint8_t rxdata;

	System_printf("Pass: 1\r\n");

	/* Write */
	for(iter=0;iter<NTEST;iter++)
    {

     	addr = iter;
        txdata = iter;

        txBuffer[0] = (addr>>8)&0xFF;
        txBuffer[1] = addr&0xFF;
        txBuffer[2] = txdata;

		i2cTransaction.slaveAddress = EEPROM_SLAVE_ADDRESS;
		i2cTransaction.writeBuf = txBuffer;
		i2cTransaction.writeCount = 3;
		i2cTransaction.readBuf = rxBuffer;
		i2cTransaction.readCount = 0;

		if (!I2C_transfer(i2c, &i2cTransaction)) {
			System_printf("I2C Transaction Failed\r\n");
			Event_post(myEvent, Event_Id_00);
			flagerr=1;
		}
		/* Write Time, could be adjusted */
		Task_sleep(10);

    }

	/* Read and Test */
	for(iter=0;iter<NTEST;iter++)
    {

    	addr = iter;
    	txdata = iter;

        txBuffer[0] = (addr>>8)&0xFF;
        txBuffer[1] = addr&0xFF;

		i2cTransaction.slaveAddress = EEPROM_SLAVE_ADDRESS;
		i2cTransaction.writeBuf = txBuffer;
		i2cTransaction.writeCount = 2;
		i2cTransaction.readBuf = rxBuffer;
		i2cTransaction.readCount = 1;

		if (I2C_transfer(i2c, &i2cTransaction)) {
			rxdata = rxBuffer[0];
			if (rxdata != txdata) {
				Event_post(myEvent, Event_Id_00);
				flagerr=1;
			}
			//System_printf("Read: %x\r\n",rxdata);
		}else{
			System_printf("I2C Transaction Failed\r\n");
			Event_post(myEvent, Event_Id_00);
			flagerr=1;
		}

    }

	System_printf("Pass: 2\r\n");

	/* Write (Complementary) */
	for(iter=0;iter<NTEST;iter++)
    {

     	addr = iter;
        txdata = (~iter)&0xFF;

        txBuffer[0] = (addr>>8)&0xFF;
        txBuffer[1] = addr&0xFF;
        txBuffer[2] = txdata;

		i2cTransaction.slaveAddress = EEPROM_SLAVE_ADDRESS;
		i2cTransaction.writeBuf = txBuffer;
		i2cTransaction.writeCount = 3;
		i2cTransaction.readBuf = rxBuffer;
		i2cTransaction.readCount = 0;

		if (!I2C_transfer(i2c, &i2cTransaction)) {
			System_printf("I2C Transaction Failed\r\n");
			Event_post(myEvent, Event_Id_00);
			flagerr=1;
		}
		Task_sleep(10);

    }

	/* Read and Test */
	for(iter=0;iter<NTEST;iter++)
    {

    	addr = iter;
    	txdata = (~iter)&0xFF;


        txBuffer[0] = (addr>>8)&0xFF;
        txBuffer[1] = addr&0xFF;

		i2cTransaction.slaveAddress = EEPROM_SLAVE_ADDRESS;
		i2cTransaction.writeBuf = txBuffer;
		i2cTransaction.writeCount = 2;
		i2cTransaction.readBuf = rxBuffer;
		i2cTransaction.readCount = 1;

		if (I2C_transfer(i2c, &i2cTransaction)) {
			rxdata = rxBuffer[0];
			if (rxdata != txdata) {
				Event_post(myEvent, Event_Id_00);
				flagerr=1;
			}
			//System_printf("Read: %x\r\n",rxdata);
		}else{
			System_printf("I2C Transaction Failed\r\n");
			Event_post(myEvent, Event_Id_00);
			flagerr=1;
		}

    }

	/* OK! */
	if(flagerr==0){
		Event_post(myEvent, Event_Id_01);
		System_printf("***\r\n");
		System_flush();
	}else{
		System_printf("KO!\r\n");
		System_flush();
	}

	Task_sleep((UInt)arg0);

    /* Deinitialized I2C */
    I2C_close(i2c);
}

/*
 *  ======== heartBeatFxn ========
 *  Toggle the Board_LED0. The Task_sleep is determined by arg0 which
 *  is configured for the heartBeat Task instance.
 */
Void heartBeatFxn(UArg arg0, UArg arg1)
{

	UInt events;

	events=Event_pend(myEvent,Event_Id_NONE,Event_Id_00+Event_Id_01,BIOS_WAIT_FOREVER);

	if(events & Event_Id_00){
	  while (1)
	  {
        Task_sleep((UInt)arg0);
        GPIO_toggle(Board_LED0);
      }
	}

	if(events & Event_Id_01){
      while (1)
      {
        Task_sleep((UInt)arg0);
        GPIO_toggle(Board_LED1);
      }
	}
}



/*
 *  ======== main ========
 */


int main(void)
{
    Task_Params taskParams;

    Semaphore_Params semParams;


    /* Call board init functions */
    Board_initGPIO();
    Board_initGeneral();
    Board_initI2C();
    // Board_initSPI();
    Board_initUART();
    // Board_initWatchdog();

    /* Default instance configuration params */
    Error_init(&eb);
    myEvent = Event_create(NULL,&eb);
    if (myEvent == NULL) {
        System_abort("Event create failed");
    }


    UART_Params uartParams;
    UART_Params_init(&uartParams);
    uartParams.baudRate = 115200;
    uartParams.readEcho = UART_ECHO_OFF;
    UartPrintf_init(UART_open(Board_UART, &uartParams));

    /* Construct Test Task thread */
    Task_Params_init(&taskParams);
    taskParams.arg0 = 100000 / Clock_tickPeriod;
    taskParams.stackSize = TASKSTACKSIZE;
    taskParams.stack = &task1Stack;
    Task_construct(&task1Struct, (Task_FuncPtr)taskFxn, &taskParams, NULL);


    /* Construct heartBeat Task  thread */
    Task_Params_init(&taskParams);
    taskParams.arg0 = 100000 / Clock_tickPeriod;
    taskParams.stackSize = TASKSTACKSIZE;
    taskParams.stack = &task0Stack;
    Task_construct(&task0Struct, (Task_FuncPtr)heartBeatFxn, &taskParams, NULL);

    /* Construct Uart Task  thread */
    Task_Params_init(&taskParams);
    taskParams.arg0 = 100000 / Clock_tickPeriod;
    taskParams.stackSize = TASKSTACKSIZE;
    taskParams.stack = &task2Stack;
    Task_construct(&task2Struct, (Task_FuncPtr)uartFxn, &taskParams, NULL);


    /* Construct Semaphore and Obtain instance handle */
    Semaphore_Params_init(&semParams);
    Semaphore_construct(&semStruct,0, &semParams);
    semHandle = Semaphore_handle(&semStruct);


    /* Setup callback for button pins */
    GPIO_setCallback(Board_BUTTON0,buttonCallbackFxn);
    GPIO_enableInt(Board_BUTTON0);

    /* Setup callback for button pins */
    GPIO_write(Board_LED0, 0);
    GPIO_write(Board_LED1, 0);

    System_printf("Test\n");

    /* Start BIOS */
    BIOS_start();

    return (0);
}
