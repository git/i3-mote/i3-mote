/*******************************************************************************
 * MSP432 Clock System - HFXT Startup
 *
 * Description:
 *
 * HFXTAL:
 * Starts the 48MHz crystal attached to HFXTIN/HFXTOUT
 * Sources MCLK from the crystal
 *
 * LFXTL:
 * Starts the 32khz crystal attached to LFXTIN/LFXTOUT
 * Sources AMCLK from crystal
 *
 * Internal DCO:
 * Starts the DCO
 * Sources SMCLK from DCO
 *
 * Blinks LEDs using SysTick (which is sourced from MCLK).
 *
 *
 *
 *              MSP432P401
 *             ------------------
 *         /|\|                  |
 *          | |                  |
 *          --|RST         P8.5  |---> LED
 *            |                  |
 *            |      PJ.3 HFXTIN |<--------
 *            |                  |   < 48Mhz xTal >
 *            |     PJ.2 HFXTOUT |<--------
 *            |                  |
 *            |       PJ.0 LFXIN |<--------
 *            |                  |   < 32khz xTal >
 *            |      PJ.1 LFXOUT |<--------
 *            |                  |
 *            |             P4.2 |--> ACLK   - 32.768 KHz
 *            |             P4.3 |--> MCLK   - 12.000 MHz
 *            |             P4.4 |--> HSMCLK -  6.000 MHz
 *            |             P7.0 |--> SMCLK
 *            |                  |
 *
 * Author: Timothy Logan
 * Rev: B.Martinez
 ******************************************************************************/
#include <MSP432_I3M_driverlib.h>

/* DriverLib Includes */
#include "driverlib.h"

/* Standard Includes */
#include <stdio.h>
#include <stdlib.h>
#include <stdint.h>
#include <stdbool.h>

#define DEBUG_UART

#ifdef UART_BAUD_9600
	const eUSCI_UART_Config uartConfig =
	{
		EUSCI_A_UART_CLOCKSOURCE_SMCLK,          // SMCLK Clock Source
		78,                                      // BRDIV = 78
		2,                                       // UCxBRF = 2
		0,                                       // UCxBRS = 0
		EUSCI_A_UART_NO_PARITY,                  // No Parity
		EUSCI_A_UART_LSB_FIRST,                  // LSB First
		EUSCI_A_UART_ONE_STOP_BIT,               // One stop bit
		EUSCI_A_UART_MODE,                       // UART mode
		EUSCI_A_UART_OVERSAMPLING_BAUDRATE_GENERATION  // Oversampling
	};
#endif

#ifdef UART_BAUD_115200
	const eUSCI_UART_Config uartConfig =
	{
		EUSCI_A_UART_CLOCKSOURCE_SMCLK,          // SMCLK Clock Source
		6,                                       // BRDIV = 78
		8,                                       // UCxBRF = 2
		0,                                       // UCxBRS = 0
		EUSCI_A_UART_NO_PARITY,                  // No Parity
		EUSCI_A_UART_LSB_FIRST,                  // LSB First
		EUSCI_A_UART_ONE_STOP_BIT,               // One stop bit
		EUSCI_A_UART_MODE,                       // UART mode
		EUSCI_A_UART_OVERSAMPLING_BAUDRATE_GENERATION  // Oversampling
	};
#endif



/*
 *  Timer_A0 (Fast)
 *  UpMode Configuration Parameter
 */
const Timer_A_UpModeConfig upConfigA0 =
{
	TIMER_A_CLOCKSOURCE_SMCLK,             // SMCLK Clock Source
	TIMER_A_CLOCKSOURCE_DIVIDER_1,         // SMCLK/1 = 3MHz
	6000-1,                                // 6000000/12000 > 1ms tick period
	TIMER_A_TAIE_INTERRUPT_DISABLE,        // Disable Timer interrupt
	TIMER_A_CCIE_CCR0_INTERRUPT_ENABLE ,   // Enable CCR0 interrupt
	TIMER_A_DO_CLEAR                       // Clear value
};


/*
 *  Timer_A (Slow)
 *  UpMode Configuration Parameter
 */
const Timer_A_UpModeConfig upConfigA1 =
{
	TIMER_A_CLOCKSOURCE_ACLK,              // ACLK Clock Source
	TIMER_A_CLOCKSOURCE_DIVIDER_1,         // ACLK/1 = 3MHz
	32768-1,
	//32-1,                                // 32768/32=1024 tick period
	TIMER_A_TAIE_INTERRUPT_DISABLE,        // Disable Timer interrupt
	TIMER_A_CCIE_CCR0_INTERRUPT_ENABLE ,   // Enable CCR0 interrupt
	TIMER_A_DO_CLEAR                       // Clear value
};


uint16_t count;
uint16_t count_tmp;

#define MAXLEN 32
char tmp[MAXLEN];

int main(void)
{

	int i=0;

    /* Halting the Watchdog */
    MAP_WDT_A_holdTimer();
    
    /* Configuring pins for peripheral/crystal usage*/
    /* LFXTAL */
    MAP_GPIO_setAsPeripheralModuleFunctionOutputPin(LFXTAL_PORT,
    		LFXTAL_PINS, GPIO_PRIMARY_MODULE_FUNCTION);

    /* HFXTL */
    MAP_GPIO_setAsPeripheralModuleFunctionOutputPin(HFXTAL_PORT,
    		HFXTAL_PINS, GPIO_PRIMARY_MODULE_FUNCTION);

    /* Clocks Output */
    GPIO_setAsPeripheralModuleFunctionOutputPin(ACLK_OUT_PORT,
    		ACLK_OUT_PIN, GPIO_PRIMARY_MODULE_FUNCTION);
    GPIO_setAsPeripheralModuleFunctionOutputPin(MCLK_OUT_PORT,
        	MCLK_OUT_PIN, GPIO_PRIMARY_MODULE_FUNCTION);
    GPIO_setAsPeripheralModuleFunctionOutputPin(HSMCLK_OUT_PORT,
            HSMCLK_OUT_PIN, GPIO_PRIMARY_MODULE_FUNCTION);
    GPIO_setAsPeripheralModuleFunctionOutputPin(SMCLK_OUT_PORT,
    		SMCLK_OUT_PIN, GPIO_PRIMARY_MODULE_FUNCTION);

    /* LEDs */
    MAP_GPIO_setAsOutputPin(HID_PORT,LEDG|LEDR);
    MAP_GPIO_setOutputHighOnPin(HID_PORT,LEDG|LEDR);

    /* Setting the external clock frequency. This API is optional */
    CS_setExternalClockSourceFrequency(32768,48000000);

    /* Starting HFXT in non-bypass mode without a timeout. Before we start
     * we have to change VCORE to 1 to support the 48MHz frequency */
    MAP_PCM_setCoreVoltageLevel(PCM_VCORE1);
    MAP_FlashCtl_setWaitState(FLASH_BANK0, 2);
    MAP_FlashCtl_setWaitState(FLASH_BANK1, 2);
    CS_startHFXT(false);


    /* Initializes other Clocks with DCO */
    MAP_CS_setDCOCenteredFrequency(CS_DCO_FREQUENCY_48);

    /* Initializing MCLK DCO/4 = 12 */
    MAP_CS_initClockSignal(CS_MCLK, CS_DCOCLK_SELECT, CS_CLOCK_DIVIDER_4);

    /* Initializing SMCLK to HFXT (effectively 48/8=6 MHz) */
    MAP_CS_initClockSignal(CS_SMCLK,CS_HFXTCLK_SELECT,CS_CLOCK_DIVIDER_8);

    /* Initializing ACLK to LFXT */
    CS_startLFXT(false);
    MAP_CS_initClockSignal(CS_ACLK, CS_LFXTCLK_SELECT, CS_CLOCK_DIVIDER_1);

    /* HSMCLK DCO/16 = 3 */
    MAP_CS_initClockSignal(CS_HSMCLK, CS_DCOCLK_SELECT, CS_CLOCK_DIVIDER_16);



#ifdef DEBUG_UART

    /* Selecting P1.2 and P1.3 in UART mode */
    MAP_GPIO_setAsPeripheralModuleFunctionInputPin(UART_PORT,UART_RX_PIN | UART_TX_PIN, GPIO_PRIMARY_MODULE_FUNCTION);
    /* Configuring UART Module */
    MAP_UART_initModule(EUSCI_A0_BASE, &uartConfig);
    /* Enable UART module */
    MAP_UART_enableModule(EUSCI_A0_BASE);

    /* Enabling UART interrupts
     * MAP_UART_enableInterrupt(EUSCI_A0_BASE, EUSCI_A_UART_RECEIVE_INTERRUPT);
     * MAP_Interrupt_enableInterrupt(INT_EUSCIA0);
     */
#endif

#ifdef DEBUG_UART

    /* Set Clock to DCO */
    MAP_CS_initClockSignal(CS_SMCLK,CS_DCOCLK_SELECT,CS_CLOCK_DIVIDER_4);

    /* Launch test */
    while(MAP_UART_receiveData(EUSCI_A0_BASE)!='g');
    MAP_GPIO_setOutputLowOnPin(HID_PORT,LEDG);
    MAP_GPIO_setOutputHighOnPin(HID_PORT,LEDR);


    /* Set SMCLK to test HFXTAL from Timer */
    MAP_CS_initClockSignal(CS_SMCLK,CS_HFXTCLK_SELECT,CS_CLOCK_DIVIDER_8);

#endif

    /*
     * Configuring SysTick to trigger at 12000000
     * MCLK is 12MHz so this will make it toggle every 1s
     *
		MAP_SysTick_enableModule();
		MAP_SysTick_setPeriod(12000000);
		MAP_SysTick_enableInterrupt();
	*/

    /* Configuring Timer_A0 for Up Mode, with SMCLK (HFXTAL) */
    MAP_Timer_A_configureUpMode(TIMER_A0_BASE, &upConfigA0);
    /* Enabling interrupts and starting the timer */
    MAP_Interrupt_enableInterrupt(INT_TA0_0);
    MAP_Timer_A_startCounter(TIMER_A0_BASE, TIMER_A_UP_MODE);

    /* Configuring Timer_A1 for Up Mode with ACLK (LFXTAL) */
    MAP_Timer_A_configureUpMode(TIMER_A1_BASE, &upConfigA1);
    /* Enabling interrupts and starting the timer */
    MAP_Interrupt_enableInterrupt(INT_TA1_0);
    MAP_Timer_A_startCounter(TIMER_A1_BASE, TIMER_A_UP_MODE);

    MAP_Interrupt_enableSleepOnIsrExit();
    MAP_Interrupt_enableMaster();
    MAP_GPIO_setOutputLowOnPin(HID_PORT,LEDG);

	MAP_PCM_gotoLPM0();

	/* Restore DCLK for UART */
	MAP_CS_initClockSignal(CS_SMCLK,CS_DCOCLK_SELECT,CS_CLOCK_DIVIDER_4);
	sprintf(tmp,"Timer-A Ticks: %d\r\n",count_tmp);
	while(tmp[i]!='\0'){
		MAP_UART_transmitData(EUSCI_A0_BASE,tmp[i]);
		i++;
	}

	/* Do Nothing */
    while(1){
    	 MAP_PCM_gotoLPM0();
    	 MAP_GPIO_toggleOutputOnPin(HID_PORT,LEDR);
    }

}


void SysTick_Handler(void)
{
    //MAP_UART_transmitData(EUSCI_A0_BASE, (count_tmp>>8)&0xFF);
    //MAP_UART_transmitData(EUSCI_A0_BASE, (count_tmp)&0xFF);
}

//******************************************************************************
// TIMER
//******************************************************************************
void TA0_0_IRQHandler(void)
{
    MAP_Timer_A_clearCaptureCompareInterrupt(TIMER_A0_BASE,TIMER_A_CAPTURECOMPARE_REGISTER_0);
    MAP_GPIO_toggleOutputOnPin(HID_PORT, LEDR);
    count++;
}

void TA1_0_IRQHandler(void)
{
    MAP_Timer_A_clearCaptureCompareInterrupt(TIMER_A1_BASE,TIMER_A_CAPTURECOMPARE_REGISTER_0);
    MAP_GPIO_toggleOutputOnPin(HID_PORT,LEDG);

	count_tmp=count;
	count=0;

	/* Disable Ints */
    MAP_Interrupt_disableInterrupt(INT_TA1_0);
    MAP_Interrupt_disableInterrupt(INT_TA0_0);
    /* Return Control to Main Program */
    MAP_Interrupt_disableSleepOnIsrExit();

}


