/*
 *  ======== empty_min.c ========
 */
/* XDCtools Header files */
#include <xdc/std.h>
#include <xdc/runtime/System.h>
#include <xdc/runtime/Error.h>

/* BIOS Header files */
#include <ti/sysbios/BIOS.h>
#include <ti/sysbios/knl/Task.h>
#include <ti/sysbios/knl/Clock.h>
#include <ti/sysbios/knl/Semaphore.h>
#include <ti/sysbios/knl/Event.h>

/* TI-RTOS Header files */
//#include <ti/drivers/I2C.h>
#include <ti/drivers/PIN.h>
// #include <ti/drivers/SPI.h>
#include <ti/drivers/UART.h>
// #include <ti/drivers/Watchdog.h>

/* Board Header files */
#include "Board.h"

#include "uart_printf.h"

#define TASKSTACKSIZE   (1024)

Task_Struct task0Struct;
Char task0Stack[TASKSTACKSIZE];

/* Semaphore */
Semaphore_Struct semStruct;
Semaphore_Handle semHandle;


/* Pin driver handle */
static PIN_Handle ledPinHandle;
static PIN_State ledPinState;

/*
 * Application LED pin configuration table:
 *   - All LEDs board LEDs are off.
 */
PIN_Config ledPinTable[] = {
    Board_LED0 | PIN_GPIO_OUTPUT_EN | PIN_GPIO_LOW | PIN_PUSHPULL | PIN_DRVSTR_MAX,
    Board_LED1 | PIN_GPIO_OUTPUT_EN | PIN_GPIO_LOW | PIN_PUSHPULL | PIN_DRVSTR_MAX,
    PIN_TERMINATE
};

/* Global memory storage for a PIN_Config table */
static PIN_Handle buttonPinHandle;
static PIN_State buttonPinState;

PIN_Config buttonPinTable[] = {
    Board_BUTTON0  | PIN_INPUT_EN | PIN_PULLUP | PIN_IRQ_NEGEDGE,
    //Board_BUTTON1  | PIN_INPUT_EN | PIN_PULLUP | PIN_IRQ_NEGEDGE,
    PIN_TERMINATE
};


void buttonCallbackFxn(PIN_Handle handle, PIN_Id pinId) {

	Semaphore_post(semHandle);
	System_printf("***\r\n");
	System_flush();

}

/*
 *  ======== heartBeatFxn ========
 *  Toggle the Board_LED0. The Task_sleep is determined by arg0 which
 *  is configured for the heartBeat Task instance.
 */
Void heartBeatFxn(UArg arg0, UArg arg1)
{

    //PIN_setOutputValue(ledPinHandle, Board_LED0,1);
    //PIN_setOutputValue(ledPinHandle, Board_LED1,1);

    Semaphore_pend(semHandle, BIOS_WAIT_FOREVER);

	/* Start test */
    //PIN_setOutputValue(ledPinHandle, Board_LED0,0);
    //PIN_setOutputValue(ledPinHandle, Board_LED1,0);

    while (1)
    {
        Task_sleep((UInt)arg0);
        PIN_setOutputValue(ledPinHandle, Board_LED1,!PIN_getOutputValue(Board_LED1));
        PIN_setOutputValue(ledPinHandle, Board_LED0,!PIN_getOutputValue(Board_LED0));
    }

}



/*
 *  ======== main ========
 */


int main(void)
{
    Task_Params taskParams;

    Semaphore_Params semParams;

    /* Call board init functions */
    Board_initGeneral();
    //Board_initI2C();
    // Board_initSPI();
    Board_initUART();
    // Board_initWatchdog();


    UART_Params uartParams;
    UART_Params_init(&uartParams);
    uartParams.baudRate = 115200;
    //uartParams.readEcho = UART_ECHO_OFF;
    UartPrintf_init(UART_open(Board_UART, &uartParams));

    /* Construct heartBeat Task  thread */
    Task_Params_init(&taskParams);
    taskParams.arg0 = 100000 / Clock_tickPeriod;
    taskParams.stackSize = TASKSTACKSIZE;
    taskParams.stack = &task0Stack;
    Task_construct(&task0Struct, (Task_FuncPtr)heartBeatFxn, &taskParams, NULL);


    /* Construct Semaphore and Obtain instance handle */
    Semaphore_Params_init(&semParams);
    Semaphore_construct(&semStruct,0, &semParams);
    semHandle = Semaphore_handle(&semStruct);

    /* Setup callback for button pins */
    buttonPinHandle = PIN_open(&buttonPinState, buttonPinTable);
    if(!buttonPinHandle) {
        System_abort("Error initializing button pins\n");
    }
    if (PIN_registerIntCb(buttonPinHandle, &buttonCallbackFxn) != 0) {
        System_abort("Error registering button callback function");
    }

    /* Open LED pins */
    ledPinHandle = PIN_open(&ledPinState, ledPinTable);
    if(!ledPinHandle) {
        System_abort("Error initializing board LED pins\n");
    }

    PIN_setOutputValue(ledPinHandle, Board_LED0,0);
    PIN_setOutputValue(ledPinHandle, Board_LED1,0);

    /* Start BIOS */
    BIOS_start();

    return (0);
}
