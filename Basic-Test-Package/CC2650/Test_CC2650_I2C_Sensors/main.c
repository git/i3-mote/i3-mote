/*
 *  ======== empty_min.c ========
 */
/* XDCtools Header files */
#include <xdc/std.h>
#include <xdc/runtime/System.h>
#include <xdc/runtime/Error.h>

/* BIOS Header files */
#include <ti/sysbios/BIOS.h>
#include <ti/sysbios/knl/Task.h>
#include <ti/sysbios/knl/Clock.h>
#include <ti/sysbios/knl/Semaphore.h>
#include <ti/sysbios/knl/Event.h>

/* TI-RTOS Header files */
#include <ti/drivers/I2C.h>
#include <ti/drivers/PIN.h>
// #include <ti/drivers/SPI.h>
#include <ti/drivers/UART.h>
// #include <ti/drivers/Watchdog.h>

/* Board Header files */
#include "Board.h"

#include "uart_printf.h"

#define TASKSTACKSIZE   (1024)

Task_Struct task0Struct;
Char task0Stack[TASKSTACKSIZE];

Task_Struct task1Struct;
Char task1Stack[TASKSTACKSIZE];

Task_Struct task2Struct;
Char task2Stack[TASKSTACKSIZE];

#define OPT3001
#define HDC1080
#define BMP280
#define TMP007

#undef MPU9250
#define LISHH12


/* Semaphore */
Semaphore_Struct semStruct;
Semaphore_Handle semHandle;

Event_Handle myEvent;
Error_Block eb;

/* Pin driver handle */
static PIN_Handle ledPinHandle;
static PIN_State ledPinState;

/*
 * Application LED pin configuration table:
 *   - All LEDs board LEDs are off.
 */
PIN_Config ledPinTable[] = {
    Board_LED0 | PIN_GPIO_OUTPUT_EN | PIN_GPIO_LOW | PIN_PUSHPULL | PIN_DRVSTR_MAX,
    Board_LED1 | PIN_GPIO_OUTPUT_EN | PIN_GPIO_LOW | PIN_PUSHPULL | PIN_DRVSTR_MAX,
    PIN_TERMINATE
};

/* Global memory storage for a PIN_Config table */
static PIN_Handle buttonPinHandle;
static PIN_State buttonPinState;

PIN_Config buttonPinTable[] = {
    Board_BUTTON0  | PIN_INPUT_EN | PIN_PULLUP | PIN_IRQ_NEGEDGE,
    //Board_BUTTON1  | PIN_INPUT_EN | PIN_PULLUP | PIN_IRQ_NEGEDGE,
    PIN_TERMINATE
};


void buttonCallbackFxn(PIN_Handle handle, PIN_Id pinId) {

	CPUdelay(8000*200);

	//Event_post(myEvent,Event_Id_02);
	Semaphore_post(semHandle);

}


Void uartFxn(UArg arg0, UArg arg1)
{
	char input;

	while (1) {
		// Blocking Read
		input=uart_getch();
		if(input=='g'){
		    //Event_post(myEvent,Event_Id_02);
		    Semaphore_post(semHandle);
		}
		Task_sleep((UInt)arg0);
	}

}

Void taskFxn(UArg arg0, UArg arg1)
{
	uint8_t         txBuffer[1];
    uint8_t         rxBuffer[2];
    I2C_Handle      i2c;
    I2C_Params      i2cParams;
    I2C_Transaction i2cTransaction;

	int flagerr=0;

    //UInt events;

    /* Create I2C for usage */
    I2C_Params_init(&i2cParams);
    i2cParams.bitRate = I2C_100kHz;
    i2c = I2C_open(Board_I2C0, &i2cParams);
    if (i2c == NULL) {
        System_abort("Error Initializing I2C\r\n");
    }
    System_printf("I2C Initialized\r\n");

    /* Wait Semaphore */
    System_printf("Press Button:\r\n");

    PIN_setOutputValue(ledPinHandle, Board_LED0,1);
    PIN_setOutputValue(ledPinHandle, Board_LED1,1);

    Semaphore_pend(semHandle, BIOS_WAIT_FOREVER);

	/* Start test */
    PIN_setOutputValue(ledPinHandle, Board_LED0,0);
    PIN_setOutputValue(ledPinHandle, Board_LED1,0);

	//while(1)
    {

#ifdef OPT3001
    	/* OPT3001 */
        txBuffer[0] = 0x7E;
		i2cTransaction.slaveAddress = Board_OPT3001_ADDR;
		i2cTransaction.writeBuf = txBuffer;
		i2cTransaction.writeCount = 1;
		i2cTransaction.readBuf = rxBuffer;
		i2cTransaction.readCount = 2;

		if (I2C_transfer(i2c, &i2cTransaction)) {
			uint16_t data = (rxBuffer[0] << 8) | (rxBuffer[1] << 0);
			if (data != 0x5449) {
				Event_post(myEvent, Event_Id_00);
				flagerr=1;
			}
			System_printf("OPT3001 Device ID [0x5449]: %x\r\n",data);
		}else{
			System_printf("OPT3001 I2c Transaction Failed\r\n");
			Event_post(myEvent, Event_Id_00);
			flagerr=1;
		}

#endif

#ifdef HDC1080
		//txBuffer[0] = 0xFE;
        txBuffer[0] = 0xFF;
		i2cTransaction.slaveAddress = Board_HDC1080_ADDR;
		i2cTransaction.writeBuf = txBuffer;
		i2cTransaction.writeCount = 1;
		i2cTransaction.readBuf = rxBuffer;
		i2cTransaction.readCount = 2;

		if (I2C_transfer(i2c, &i2cTransaction)) {
			uint16_t data = (rxBuffer[0] << 8) | (rxBuffer[1] << 0);
			if (data != 0x1050) {
				Event_post(myEvent, Event_Id_00);
				flagerr=1;
			}
			//System_printf("HDC1080 Manufactured ID 0x5449: %x\n",data);
			System_printf("HDC1080 Device ID [0x1050]: %x\r\n",data);
		}
		else{
			System_printf("HDC1080 I2c Transaction Failed\r\n");
			Event_post(myEvent, Event_Id_00);
			flagerr=1;
		}
#endif

#ifdef TMP007
		/*
        txBuffer[0] = 0x1E;
		i2cTransaction.slaveAddress = Board_TMP007_ADDR;
		i2cTransaction.writeBuf = txBuffer;
		i2cTransaction.writeCount = 1;
		i2cTransaction.readBuf = rxBuffer;
		i2cTransaction.readCount = 2;

		if (I2C_transfer(i2c, &i2cTransaction)) {
			uint16_t data = (rxBuffer[0] << 8) | (rxBuffer[1] << 0);
			if (data != 0x5449) {
				Event_post(myEvent, Event_Id_00);
				flagerr=1;
			}
			System_printf("TMP007 Manufactured ID 0x5449: %x\r\n",data);
		}else{
		    Event_post(myEvent, Event_Id_00);
		    flagerr=1;
		}

		*/

        txBuffer[0] = 0x1F;
		i2cTransaction.slaveAddress = Board_TMP007_ADDR;
		i2cTransaction.writeBuf = txBuffer;
		i2cTransaction.writeCount = 1;
		i2cTransaction.readBuf = rxBuffer;
		i2cTransaction.readCount = 2;

		if (I2C_transfer(i2c, &i2cTransaction)) {
			uint16_t data = (rxBuffer[0] << 8) | (rxBuffer[1] << 0);
			if (data != 0x0078) {
				Event_post(myEvent, Event_Id_00);
				flagerr=1;
			}
			System_printf("TMP007 Device ID [0x0078]: %x\r\n",data);
		}else{
			System_printf("TMP007 I2c Transaction Failed\r\n");
			Event_post(myEvent, Event_Id_00);
			flagerr=1;
		}

#endif

#ifdef BMP280
	    /* BMP280 */
        txBuffer[0] = 0xD0;
		i2cTransaction.slaveAddress = Board_BMP280_ADDR;
		i2cTransaction.writeBuf = txBuffer;
		i2cTransaction.writeCount = 1;
		i2cTransaction.readBuf = rxBuffer;
		i2cTransaction.readCount = 1;

		if (I2C_transfer(i2c, &i2cTransaction)) {
			uint16_t data = (rxBuffer[0]);
			if (data != 0x58) {
				Event_post(myEvent, Event_Id_00);
				flagerr=1;
			}
			System_printf("BMP280 Device ID [0x58]: %x\r\n",data);
		}else{
			System_printf("BMP280 I2c Transaction Failed\r\n");
			Event_post(myEvent, Event_Id_00);
			flagerr=1;
		}
#endif

#ifdef MPU9250
		txBuffer[0] = 0x75;
		i2cTransaction.slaveAddress = Board_MPU9250_ADDR;
		i2cTransaction.writeBuf = txBuffer;
		i2cTransaction.writeCount = 1;
		i2cTransaction.readBuf = rxBuffer;
		i2cTransaction.readCount = 1;

		if (I2C_transfer(i2c, &i2cTransaction)) {
			uint8_t data = rxBuffer[0];
			if (data != 0x71) {
				Event_post(myEvent, Event_Id_00);
				flagerr=1;
			}
			System_printf("MPU9250 WHOIAM [0x71]: %x\r\n",data);
		}else{
			System_printf("MPU9250 I2c Transaction Failed\r\n");
			Event_post(myEvent, Event_Id_00);
			flagerr=1;
		}
#endif

#ifdef LISHH12

		txBuffer[0] = 0x0F;
		i2cTransaction.slaveAddress = Board_LISHH12_ADDR;
		i2cTransaction.writeBuf = txBuffer;
		i2cTransaction.writeCount = 1;
		i2cTransaction.readBuf = rxBuffer;
		i2cTransaction.readCount = 1;

		if (I2C_transfer(i2c, &i2cTransaction)) {
			uint8_t data = rxBuffer[0];
			if (data != 0x41) {
				Event_post(myEvent, Event_Id_00);
				flagerr=1;
			}
			System_printf("LISHH12 WHOIAM [0x41]: %x\r\n",data);
		}else{
			System_printf("LISHH12 I2c Transaction Failed\r\n");
			Event_post(myEvent, Event_Id_00);
			flagerr=1;
		}
#endif

		/* OK! */
		if(flagerr==0){
			Event_post(myEvent, Event_Id_01);
			System_printf("***\r\n");
			System_flush();
		}else{
			System_printf("KO!\r\n");
			System_flush();
		}

		Task_sleep((UInt)arg0);
    }

    /* Deinitialized I2C */
    I2C_close(i2c);
}

/*
 *  ======== heartBeatFxn ========
 *  Toggle the Board_LED0. The Task_sleep is determined by arg0 which
 *  is configured for the heartBeat Task instance.
 */
Void heartBeatFxn(UArg arg0, UArg arg1)
{

	UInt events;

	events=Event_pend(myEvent,Event_Id_NONE,Event_Id_00+Event_Id_01,BIOS_WAIT_FOREVER);

	if(events & Event_Id_00){
	  while (1)
	  {
        Task_sleep((UInt)arg0);
        PIN_setOutputValue(ledPinHandle, Board_LED0,!PIN_getOutputValue(Board_LED0));
      }
	}

	if(events & Event_Id_01){
      while (1)
      {
        Task_sleep((UInt)arg0);
        PIN_setOutputValue(ledPinHandle, Board_LED1,!PIN_getOutputValue(Board_LED1));
      }
	}
}



/*
 *  ======== main ========
 */


int main(void)
{
    Task_Params taskParams;

    Semaphore_Params semParams;

    /* Call board init functions */
    Board_initGeneral();
    Board_initI2C();
    // Board_initSPI();
    Board_initUART();
    // Board_initWatchdog();

    /* Default instance configuration params */
    Error_init(&eb);
    myEvent = Event_create(NULL,&eb);
    if (myEvent == NULL) {
        System_abort("Event create failed");
    }



    UART_Params uartParams;
    UART_Params_init(&uartParams);
    uartParams.baudRate = 115200;
    //uartParams.readEcho = UART_ECHO_OFF;
    UartPrintf_init(UART_open(Board_UART, &uartParams));

    /* Construct Test Task thread */
    Task_Params_init(&taskParams);
    taskParams.arg0 = 100000 / Clock_tickPeriod;
    taskParams.stackSize = TASKSTACKSIZE;
    taskParams.stack = &task1Stack;
    Task_construct(&task1Struct, (Task_FuncPtr)taskFxn, &taskParams, NULL);


    /* Construct heartBeat Task  thread */
    Task_Params_init(&taskParams);
    taskParams.arg0 = 100000 / Clock_tickPeriod;
    taskParams.stackSize = TASKSTACKSIZE;
    taskParams.stack = &task0Stack;
    Task_construct(&task0Struct, (Task_FuncPtr)heartBeatFxn, &taskParams, NULL);

    /* Construct Uart Task  thread */
    Task_Params_init(&taskParams);
    taskParams.arg0 = 100000 / Clock_tickPeriod;
    taskParams.stackSize = TASKSTACKSIZE;
    taskParams.stack = &task2Stack;
    Task_construct(&task2Struct, (Task_FuncPtr)uartFxn, &taskParams, NULL);


    /* Construct Semaphore and Obtain instance handle */
    Semaphore_Params_init(&semParams);
    Semaphore_construct(&semStruct,0, &semParams);
    semHandle = Semaphore_handle(&semStruct);

    /* Setup callback for button pins */
    buttonPinHandle = PIN_open(&buttonPinState, buttonPinTable);
    if(!buttonPinHandle) {
        System_abort("Error initializing button pins\n");
    }
    if (PIN_registerIntCb(buttonPinHandle, &buttonCallbackFxn) != 0) {
        System_abort("Error registering button callback function");
    }

    /* Open LED pins */
    ledPinHandle = PIN_open(&ledPinState, ledPinTable);
    if(!ledPinHandle) {
        System_abort("Error initializing board LED pins\n");
    }

    PIN_setOutputValue(ledPinHandle, Board_LED0,1);
    PIN_setOutputValue(ledPinHandle, Board_LED1,1);

    /* Start BIOS */
    BIOS_start();

    return (0);
}
