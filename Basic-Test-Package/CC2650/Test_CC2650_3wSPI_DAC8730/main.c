/*
 *  ======== empty_min.c ========
 */
/* XDCtools Header files */
#include <xdc/std.h>
#include <xdc/runtime/System.h>
#include <xdc/runtime/Error.h>

/* BIOS Header files */
#include <ti/sysbios/BIOS.h>
#include <ti/sysbios/knl/Task.h>
#include <ti/sysbios/knl/Clock.h>
#include <ti/sysbios/knl/Semaphore.h>
#include <ti/sysbios/knl/Event.h>

/* TI-RTOS Header files */
//#include <ti/drivers/I2C.h>
#include <ti/drivers/PIN.h>
#include <ti/drivers/SPI.h>
#include <ti/drivers/UART.h>
// #include <ti/drivers/Watchdog.h>

/* Board Header files */
#include "Board.h"
#include "uart_printf.h"

#define TASKSTACKSIZE   512

Task_Struct task0Struct;
Char task0Stack[TASKSTACKSIZE];

Task_Struct task1Struct;
Char task1Stack[TASKSTACKSIZE];

Task_Struct task2Struct;
Char task2Stack[TASKSTACKSIZE];



Event_Handle myEvent;
Error_Block eb;

Semaphore_Struct semStruct;
Semaphore_Handle semHandle;


/*
 * Application LED pin configuration table:
 *   - All LEDs board LEDs are off.
 */
static PIN_Handle ledPinHandle;
static PIN_State ledPinState;

PIN_Config ledPinTable[] = {
    Board_LED0 | PIN_GPIO_OUTPUT_EN | PIN_GPIO_LOW | PIN_PUSHPULL | PIN_DRVSTR_MAX,
    Board_LED1 | PIN_GPIO_OUTPUT_EN | PIN_GPIO_LOW | PIN_PUSHPULL | PIN_DRVSTR_MAX,
	PIN_TERMINATE
};

/**
static PIN_Handle csnPinHandle;
static PIN_State csnPinState;

PIN_Config csnPinTable[] = {
	Board_DAC8730_CSN| PIN_GPIO_OUTPUT_EN | PIN_GPIO_LOW | PIN_PUSHPULL | PIN_DRVSTR_MAX,
	PIN_TERMINATE
};
**/

/* Global memory storage for a PIN_Config table */
static PIN_Handle buttonPinHandle;
static PIN_State buttonPinState;

PIN_Config buttonPinTable[] = {
    Board_BUTTON0  | PIN_INPUT_EN | PIN_PULLUP | PIN_IRQ_NEGEDGE,
    //Board_BUTTON1  | PIN_INPUT_EN | PIN_PULLUP | PIN_IRQ_NEGEDGE,
    PIN_TERMINATE
};

void buttonCallbackFxn(PIN_Handle handle, PIN_Id pinId) {

	CPUdelay(8000*200);

	Semaphore_post(semHandle);
	//Event_post(myEvent,Event_Id_02);

}

Void uartFxn(UArg arg0, UArg arg1)
{
	char input;

	while (1) {
		// Blocking Read
		input=uart_getch();
		if(input=='g'){
		    //Event_post(myEvent,Event_Id_02);
		    Semaphore_post(semHandle);
		}
	    Task_sleep((UInt)arg0);
	}

}



#define REGSBW 0x1E
#define REGSBR 0x0F

#define EEPSBW 0x5A
#define EEPSBR 0x4B

#define CMRST 0xC3

#define WENA  0x00
#define VCFG1 0x01
#define VCFG2 0x01

#define STA1 0x07
#define STA2 0x08
#define GPVR 0x0C

#define CFG3 0xA6


Void spiFxn(UArg arg0, UArg arg1)
{
	uint8_t         txBufferPointer[8];
	uint8_t         rxBufferPointer[8];

	int i;
	uint8_t k=0;
	int flagerr=0;

	SPI_Handle      spi;
	SPI_Params      spiParams;
    SPI_Transaction spiTransaction;

    UInt events;

    SPI_Params_init(&spiParams);
    // Slave mode
    spiParams.mode = SPI_MASTER;
    spiParams.bitRate = 10000;
    spiParams.transferMode = SPI_MODE_BLOCKING;
    spiParams.frameFormat = SPI_POL0_PHA0;

    spi=SPI_open(Board_SPI0,&spiParams);
    if(!spi){
      System_printf("SPI did not open");
    }
    System_printf("SPI Init Done\r\n");

	//Task_sleep((UInt)arg0);

    System_printf("Press button:\r\n");
    PIN_setOutputValue(ledPinHandle, Board_LED0,1);
    PIN_setOutputValue(ledPinHandle, Board_LED1,1);

    Semaphore_pend(semHandle, BIOS_WAIT_FOREVER);

	/* Start test */
    PIN_setOutputValue(ledPinHandle, Board_LED0,0);
    PIN_setOutputValue(ledPinHandle, Board_LED1,0);

	/* Send SPI Reset Command */

	spiTransaction.rxBuf = rxBufferPointer;
	spiTransaction.txBuf = txBufferPointer;
	spiTransaction.count=3;

	txBufferPointer[0]=CMRST;
	txBufferPointer[1]=0x55;
	txBufferPointer[2]=0xFF;

	rxBufferPointer[0]=0;
	rxBufferPointer[1]=0;
	rxBufferPointer[2]=0;

	if (SPI_transfer(spi,&spiTransaction)) {
		if (rxBufferPointer[2] != 0xF0)
		{
			Event_post(myEvent, Event_Id_00);
			flagerr=1;
		}
		System_printf("RST: %x %x %x\r\n",rxBufferPointer[0],rxBufferPointer[1],rxBufferPointer[2]);
		System_flush();
	}else{
		Event_post(myEvent, Event_Id_00);
		flagerr=1;
		//Task_exit();
	}
	for(i=1000;i>0;i--);


	/* Read 4 Bytes from EEPROM: Chip ID*/
	txBufferPointer[0]=0x63;
	txBufferPointer[1]=0xA0;
	txBufferPointer[2]=0x04;
	txBufferPointer[3]=0x03;
	txBufferPointer[4]=0x02;
	txBufferPointer[5]=0x01;
	txBufferPointer[6]=0xFF;

	spiTransaction.rxBuf = rxBufferPointer;
	spiTransaction.txBuf = txBufferPointer;
	spiTransaction.count = 7;

	if (SPI_transfer(spi,&spiTransaction)) {

		if (rxBufferPointer[2] != 0xF0)
		{
			Event_post(myEvent, Event_Id_00);
			flagerr=1;
		}

		System_printf("IDC: ");
		for(i=0;i<7;i++){
			System_printf("%x ",rxBufferPointer[i]);
		}
		System_printf("\r\n");
		System_flush();
	}else{
		Event_post(myEvent, Event_Id_00);
		flagerr=1;
		//Task_exit();
	}

	/* OK! */
	if(flagerr==0){
		Event_post(myEvent, Event_Id_01);
		System_printf("***\r\n");
		System_flush();
	}else{
		System_printf("KO!\r\n");
		System_flush();
	}

    System_printf("Press button to continue:\r\n");
    Semaphore_pend(semHandle, BIOS_WAIT_FOREVER);

	while(1){

		Task_sleep((UInt)arg0);

		// Enable Write GPVR Register
		txBufferPointer[0]=REGSBW;
    	txBufferPointer[1]=WENA;
    	txBufferPointer[2]=GPVR;
    	txBufferPointer[3]=0xFF;

    	spiTransaction.rxBuf = rxBufferPointer;
    	spiTransaction.txBuf = txBufferPointer;
    	spiTransaction.count = 4;

    	System_printf("TX: %x\r\n",k);

		if (SPI_transfer(spi,&spiTransaction)) {
			System_printf("WENA: %x %x %x %x\r\n",rxBufferPointer[0],rxBufferPointer[1],rxBufferPointer[2],rxBufferPointer[3]);
			System_flush();
		}

		// Write GPVR Register
    	txBufferPointer[0]=REGSBW;
    	txBufferPointer[1]=GPVR;
    	txBufferPointer[2]=k++;
    	txBufferPointer[3]=0xFF;

    	spiTransaction.rxBuf = rxBufferPointer;
    	spiTransaction.txBuf = txBufferPointer;
    	spiTransaction.count = 4;

    	if (SPI_transfer(spi,&spiTransaction)) {
			System_printf("REGSBW: %x %x %x %x\r\n",rxBufferPointer[0],rxBufferPointer[1],rxBufferPointer[2],rxBufferPointer[3]);
			System_flush();
		}

		// Read GPVR Register
		txBufferPointer[0]=REGSBR;
    	txBufferPointer[1]=GPVR;
    	txBufferPointer[2]=0x01;
    	txBufferPointer[3]=0xFF;

    	spiTransaction.rxBuf = rxBufferPointer;
    	spiTransaction.txBuf = txBufferPointer;
    	spiTransaction.count = 4;

		if (SPI_transfer(spi,&spiTransaction)) {
			System_printf("REGSBR: %x %x %x %x\r\n",rxBufferPointer[0],rxBufferPointer[1],rxBufferPointer[2],rxBufferPointer[3]);
			System_flush();
		}

		/* READ 1 BYTE from EEPROM
		txBufferPointer[0]=EEPSBR;
		txBufferPointer[1]=0xA0;
		txBufferPointer[2]=0x01;
		txBufferPointer[3]=0xFF;

		spiTransaction.rxBuf = rxBufferPointer;
		spiTransaction.txBuf = txBufferPointer;
		spiTransaction.count = 4;

		if (SPI_transfer(spi,&spiTransaction)) {
			System_printf("ID0: %x %x %x %x\r\n",rxBufferPointer[0],rxBufferPointer[1],rxBufferPointer[2],rxBufferPointer[3]);
			System_flush();
		}
		*/

    }

    /* Deinitialized I2C */
    SPI_close(spi);
}


/*
 *  ======== heartBeatFxn ========
 *  Toggle the Board_LED0. The Task_sleep is determined by arg0 which
 *  is configured for the heartBeat Task instance.
 */
Void heartBeatFxn(UArg arg0, UArg arg1)
{

	UInt events;

	events=Event_pend(myEvent,Event_Id_NONE,Event_Id_00+Event_Id_01,BIOS_WAIT_FOREVER);

	if(events & Event_Id_00){
	  while (1)
	  {
        Task_sleep((UInt)arg0);
        PIN_setOutputValue(ledPinHandle, Board_LED1,!PIN_getOutputValue(Board_LED1));
      }
	}

	if(events & Event_Id_01){
      while (1)
      {
        Task_sleep((UInt)arg0);
        PIN_setOutputValue(ledPinHandle, Board_LED0,!PIN_getOutputValue(Board_LED0));
      }
	}
}

/*
 *  ======== main ========
 */
int main(void)
{
    Task_Params taskParams;
    Semaphore_Params semParams;

    /* Call board init functions */
    Board_initGeneral();
    Board_initSPI();
    // Board_initI2C();
    Board_initUART();
    // Board_initWatchdog();


    /* Default instance configuration params */
    Error_init(&eb);
    myEvent = Event_create(NULL,&eb);
    if (myEvent == NULL) {
        System_abort("Event create failed");
    }


    /* BMH  Init UART for System_printf() */
    UART_Params uartParams;
    UART_Params_init(&uartParams);
    uartParams.baudRate = 115200;
    UartPrintf_init(UART_open(Board_UART, &uartParams));
    System_printf("Uart open\r\n");


    /* Construct SPI Echo Task thread */
    Task_Params_init(&taskParams);
    taskParams.arg0 = 1000000 / Clock_tickPeriod;
    taskParams.stackSize = TASKSTACKSIZE;
    taskParams.stack = &task1Stack;
    Task_construct(&task1Struct, (Task_FuncPtr)spiFxn, &taskParams, NULL);


    /* Construct heartBeat Task thread */
    Task_Params_init(&taskParams);
    taskParams.arg0 = 500000 / Clock_tickPeriod;
    taskParams.stackSize = TASKSTACKSIZE;
    taskParams.stack = &task0Stack;
    Task_construct(&task0Struct, (Task_FuncPtr)heartBeatFxn, &taskParams, NULL);


    /* Construct Uart Task  thread */
    Task_Params_init(&taskParams);
    taskParams.arg0 = 100000 / Clock_tickPeriod;
    taskParams.stackSize = TASKSTACKSIZE;
    taskParams.stack = &task2Stack;
    Task_construct(&task2Struct, (Task_FuncPtr)uartFxn, &taskParams, NULL);


    /* Construct Semaphore Obtain instance handle */
    Semaphore_Params_init(&semParams);
    Semaphore_construct(&semStruct,0, &semParams);
    semHandle = Semaphore_handle(&semStruct);


    /* Setup callback for button pins */
    buttonPinHandle = PIN_open(&buttonPinState, buttonPinTable);
    if(!buttonPinHandle) {
        System_abort("Error initializing button pins\n");
    }
    if (PIN_registerIntCb(buttonPinHandle, &buttonCallbackFxn) != 0) {
        System_abort("Error registering button callback function");
    }


    /* Open LED pins */
    ledPinHandle = PIN_open(&ledPinState, ledPinTable);
    if(!ledPinHandle) {
        System_abort("Error initializing board LED pins\n");
    }

    PIN_setOutputValue(ledPinHandle, Board_LED0, 1);
    PIN_setOutputValue(ledPinHandle, Board_LED1, 1);

    /* Start BIOS */
    BIOS_start();

    return (0);
}
