/*
 *  ======== empty_min.c ========
 */
/* XDCtools Header files */
#include <xdc/std.h>
#include <xdc/runtime/System.h>

/* BIOS Header files */
#include <ti/sysbios/BIOS.h>
#include <ti/sysbios/knl/Task.h>
#include <ti/sysbios/knl/Clock.h>

/* TI-RTOS Header files */
//#include <ti/drivers/I2C.h>
#include <ti/drivers/PIN.h>
#include <ti/drivers/SPI.h>
#include <ti/drivers/UART.h>
// #include <ti/drivers/Watchdog.h>

/* Board Header files */
#include "Board.h"
#include "uart_printf.h"

#define TASKSTACKSIZE   512

Task_Struct task0Struct;
Char task0Stack[TASKSTACKSIZE];

Task_Struct task1Struct;
Char task1Stack[TASKSTACKSIZE];


/* Pin driver handle */
static PIN_Handle ledPinHandle;
static PIN_State ledPinState;

/*
 * Application LED pin configuration table:
 *   - All LEDs board LEDs are off.
 */

PIN_Config ledPinTable[] = {
    Board_LED0 | PIN_GPIO_OUTPUT_EN | PIN_GPIO_LOW | PIN_PUSHPULL | PIN_DRVSTR_MAX,
    Board_LED1 | PIN_GPIO_OUTPUT_EN | PIN_GPIO_LOW | PIN_PUSHPULL | PIN_DRVSTR_MAX,
    PIN_TERMINATE
};

Void echoFxn(UArg arg0, UArg arg1)
{
	uint8_t         rxBufferPointer[4];
	uint8_t         txBufferPointer[4];
    SPI_Handle      spi;
    SPI_Params      spiParams;
    SPI_Transaction spiTransaction;

    SPI_Params_init(&spiParams);
    // Slave mode
    spiParams.mode = SPI_SLAVE;
    spiParams.bitRate = 500000;
    spiParams.frameFormat = SPI_POL1_PHA1;
    // params.transferMode = SPI_MODE_CALLBACK;
    //spiParams.transferCallbackFxn = spi_tx_call;

    spi=SPI_open(Board_SPI1,&spiParams);
    if(!spi){
      System_printf("SPI did not open");
    }
    System_printf("SPI Slave Mode Open\r\n");

    rxBufferPointer[0]='*';

    while(1) {

    	spiTransaction.rxBuf= rxBufferPointer;
    	spiTransaction.txBuf = txBufferPointer;
    	spiTransaction.count=1;

		PIN_setOutputValue(ledPinHandle, Board_LEDR,0);

		if (SPI_transfer(spi,&spiTransaction)) {

			PIN_setOutputValue(ledPinHandle, Board_LEDR,1);

			System_printf("RxData: %x\r\n",rxBufferPointer[0]);

			txBufferPointer[0]=rxBufferPointer[0];
			rxBufferPointer[0]=0x55;

		}


    }

    /* Deinitialized I2C */
    SPI_close(spi);
}


/*
 *  ======== heartBeatFxn ========
 *  Toggle the Board_LED0. The Task_sleep is determined by arg0 which
 *  is configured for the heartBeat Task instance.
 */
Void heartBeatFxn(UArg arg0, UArg arg1)
{
    while (1) {

     	Task_sleep((UInt)arg0-5000/Clock_tickPeriod);
    	PIN_setOutputValue(ledPinHandle, Board_LEDG,1);
    	Task_sleep((UInt)5000/Clock_tickPeriod);
    	PIN_setOutputValue(ledPinHandle, Board_LEDG,0);

        //PIN_setOutputValue(ledPinHandle, Board_LEDG,!PIN_getOutputValue(Board_LEDG));
        //PIN_setOutputValue(ledPinHandle, Board_LEDR,!PIN_getOutputValue(Board_LEDR));
    }
}

/*
 *  ======== main ========
 */
int main(void)
{
    Task_Params taskParams;

    /* Call board init functions */
    Board_initGeneral();
    Board_initSPI();
    // Board_initI2C();
    // Board_initUART();
    // Board_initWatchdog();

    /* Init UART for System_printf()*/
    UART_Params uartParams;
    UART_Params_init(&uartParams);
    uartParams.baudRate = 115200;
    UartPrintf_init(UART_open(Board_UART, &uartParams));
    System_printf("Uart open\r\n");

    /* Construct heartBeat Task thread */
    Task_Params_init(&taskParams);
    taskParams.arg0 = 2000000 / Clock_tickPeriod;
    taskParams.stackSize = TASKSTACKSIZE;
    taskParams.stack = &task0Stack;
    Task_construct(&task0Struct, (Task_FuncPtr)heartBeatFxn, &taskParams, NULL);

    /* Construct SPI Echo Task thread */
    Task_Params_init(&taskParams);
    taskParams.arg0 = 0;
    taskParams.stackSize = TASKSTACKSIZE;
    taskParams.stack = &task1Stack;
    Task_construct(&task1Struct, (Task_FuncPtr)echoFxn, &taskParams, NULL);

    /* Open LED pins */
    ledPinHandle = PIN_open(&ledPinState, ledPinTable);
    if(!ledPinHandle) {
        System_abort("Error initializing board LED pins\n");
    }

    PIN_setOutputValue(ledPinHandle, Board_LED0, 0);
    PIN_setOutputValue(ledPinHandle, Board_LED1, 0);

    /* Start BIOS */
    BIOS_start();

    return (0);
}
