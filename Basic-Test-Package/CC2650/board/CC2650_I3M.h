/*
 * Copyright (c) 2015-2016, Texas Instruments Incorporated
 * All rights reserved.
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions
 * are met:
 *
 * *  Redistributions of source code must retain the above copyright
 *    notice, this list of conditions and the following disclaimer.
 *
 * *  Redistributions in binary form must reproduce the above copyright
 *    notice, this list of conditions and the following disclaimer in the
 *    documentation and/or other materials provided with the distribution.
 *
 * *  Neither the name of Texas Instruments Incorporated nor the names of
 *    its contributors may be used to endorse or promote products derived
 *    from this software without specific prior written permission.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
 * AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO,
 * THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR
 * PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT OWNER OR
 * CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL,
 * EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO,
 * PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS;
 * OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY,
 * WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR
 * OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE,
 * EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 */
/** ============================================================================
 *  @file       CC2650_I3MOTE.h
 *
 *  @brief      CC2650 LaunchPad Board Specific header file.
 *
 *  NB! This is the board file for CC2650 LaunchPad PCB version 1.1
 *
 *  ============================================================================
 */
#ifndef __CC2650_I3MOTE_BOARD_H__
#define __CC2650_I3MOTE_BOARD_H__

#ifdef __cplusplus
extern "C" {
#endif

/** ============================================================================
 *  Includes
 *  ==========================================================================*/
#include <ti/drivers/PIN.h>
#include <driverlib/ioc.h>

/** ============================================================================
 *  Externs
 *  ==========================================================================*/
extern const PIN_Config BoardGpioInitTable[];

/** ============================================================================
 *  Defines
 *  ==========================================================================*/

/* Same RF Configuration as 7x7 EM */
#define CC2650EM_7ID
#define CC2650_I3MOTE


/* Sensors */
#define Board_INA219_ADDR    (0x41)
#define Board_HDC1000_ADDR   (0x43)
#define Board_HDC1080_ADDR   (0x40)
#define Board_TMP007_ADDR    (0x44)
#define Board_OPT3001_ADDR   (0x45)
#define Board_MPU9250_ADDR   (0x68)
#define Board_BMP280_ADDR    (0x77)
#define Board_LISHH12_ADDR   (0x1E)




/* Mapping of pins to board signals using general board aliases
 *      <board signal alias>        <pin mapping>
 */
/* Discrete outputs */
/* I2C */

#define Board_I2C0_SDA0             IOID_23
#define Board_I2C0_SCL0             IOID_24

#define Board_LEDG                  IOID_6
#define Board_LEDR                  IOID_5

#define Board_LED_ON                1
#define Board_LED_OFF               0

/* Discrete inputs */
#define Board_BTN1                  IOID_4
#define Board_BTN2                  PIN_UNASSIGNED


/* DAC8730 UART */
#ifdef Board_UART1
  #warning "DAC8730-UART"
  #define Board_UART_TX               IOID_8  // TXD to DAC
  #define Board_UART_RX               IOID_9  // RXD from DAC
#else
/* Debug UART */
  #define Board_UART_RX               IOID_2
  #define Board_UART_TX               IOID_3
#endif

/* SPI Board */
#define Board_SPI0_IRQ              IOID_10
#define Board_SPI0_MISO             IOID_12
#define Board_SPI0_MOSI             IOID_13
#define Board_SPI0_CLK              IOID_14
#define Board_SPI0_CSN              IOID_15

#define Board_SPI1_MISO             IOID_18
#define Board_SPI1_MOSI             IOID_19
#define Board_SPI1_CLK              IOID_20
#define Board_SPI1_CSN              IOID_21
#define Board_SPI1_IRQ              IOID_22

/* Power */
//#define Board_DIO0                IOID_0  // VCCIO_DAC
//#define Board_DIO1                IOID_1  // GPS_PWR_nEN
//#define Board_DIO7                IOID_7  // EH_nBYPASS
//#define Board_DIO25               IOID_25 // SSM_PWR_nEN
//#define Board_DIO26               IOID_26 // GPS_PWR_nEN
//#define Board_DIO27               IOID_27 // VBAT_OK
//#define Board_DIO28               IOID_28 // SSM_5V_EN

/* Misc */
//#define Board_DIO11                 IOID_11 // BSL
//#define Board_DIO16                 IOID_16 // JTAG_TDO
//#define Board_DIO17                 IOID_17 // JTAG_TDI



/* Analog */
#define Board_DIO23_ANALOG          PIN_UNASSIGNED
#define Board_DIO24_ANALOG          PIN_UNASSIGNED
#define Board_DIO25_ANALOG          PIN_UNASSIGNED
#define Board_DIO26_ANALOG          PIN_UNASSIGNED
#define Board_DIO27_ANALOG          PIN_UNASSIGNED
#define Board_DIO28_ANALOG          PIN_UNASSIGNED
#define Board_DIO29_ANALOG          PIN_UNASSIGNED
#define Board_DIO30_ANALOG          PIN_UNASSIGNED


/* PWM outputs */
#define Board_PWMPIN0                PIN_UNASSIGNED
#define Board_PWMPIN1                PIN_UNASSIGNED
#define Board_PWMPIN2                PIN_UNASSIGNED
#define Board_PWMPIN3                PIN_UNASSIGNED
#define Board_PWMPIN4                PIN_UNASSIGNED
#define Board_PWMPIN5                PIN_UNASSIGNED
#define Board_PWMPIN6                PIN_UNASSIGNED
#define Board_PWMPIN7                PIN_UNASSIGNED


/** ============================================================================
 *  Instance identifiers
 *  ==========================================================================*/
/* Generic I2C instance identifiers */
#define Board_I2C                   CC2650_I3MOTE_I2C0
/* Generic SPI instance identifiers */
#define Board_SPI0                  CC2650_I3MOTE_SPI0
#define Board_SPI1                  CC2650_I3MOTE_SPI1
/* Generic UART instance identifiers */
#define Board_UART                  CC2650_I3MOTE_UART0
/* Generic Crypto instance identifiers */
#define Board_CRYPTO                CC2650_I3MOTE_CRYPTO0
/* Generic GPTimer instance identifiers */
#define Board_GPTIMER0A             CC2650_I3MOTE_GPTIMER0A
#define Board_GPTIMER0B             CC2650_I3MOTE_GPTIMER0B
#define Board_GPTIMER1A             CC2650_I3MOTE_GPTIMER1A
#define Board_GPTIMER1B             CC2650_I3MOTE_GPTIMER1B
#define Board_GPTIMER2A             CC2650_I3MOTE_GPTIMER2A
#define Board_GPTIMER2B             CC2650_I3MOTE_GPTIMER2B
#define Board_GPTIMER3A             CC2650_I3MOTE_GPTIMER3A
#define Board_GPTIMER3B             CC2650_I3MOTE_GPTIMER3B
/* Generic PWM instance identifiers */
#define Board_PWM0                  CC2650_I3MOTE_PWM0
#define Board_PWM1                  CC2650_I3MOTE_PWM1
#define Board_PWM2                  CC2650_I3MOTE_PWM2
#define Board_PWM3                  CC2650_I3MOTE_PWM3
#define Board_PWM4                  CC2650_I3MOTE_PWM4
#define Board_PWM5                  CC2650_I3MOTE_PWM5
#define Board_PWM6                  CC2650_I3MOTE_PWM6
#define Board_PWM7                  CC2650_I3MOTE_PWM7

/** ============================================================================
 *  Number of peripherals and their names
 *  ==========================================================================*/

/*!
 *  @def    CC2650_I3MOTE_I2CName
 *  @brief  Enum of I2C names on the CC2650 dev board
 */
typedef enum CC2650_I3MOTE_I2CName {
    CC2650_I3MOTE_I2C0 = 0,

    CC2650_I3MOTE_I2CCOUNT
} CC2650_I3MOTE_I2CName;

/*!
 *  @def    CC2650_I3MOTE_CryptoName
 *  @brief  Enum of Crypto names on the CC2650 dev board
 */
typedef enum CC2650_I3MOTE_CryptoName {
    CC2650_I3MOTE_CRYPTO0 = 0,

    CC2650_I3MOTE_CRYPTOCOUNT
} CC2650_I3MOTE_CryptoName;


/*!
 *  @def    CC2650_I3MOTE_SPIName
 *  @brief  Enum of SPI names on the CC2650 dev board
 */
typedef enum CC2650_I3MOTE_SPIName {
    CC2650_I3MOTE_SPI0 = 0,
    CC2650_I3MOTE_SPI1,

    CC2650_I3MOTE_SPICOUNT
} CC2650_I3MOTE_SPIName;

/*!
 *  @def    CC2650_I3MOTE_UARTName
 *  @brief  Enum of UARTs on the CC2650 dev board
 */
typedef enum CC2650_I3MOTE_UARTName {
    CC2650_I3MOTE_UART0 = 0,

    CC2650_I3MOTE_UARTCOUNT
} CC2650_I3MOTE_UARTName;

/*!
 *  @def    CC2650_I3MOTE_UdmaName
 *  @brief  Enum of DMA buffers
 */
typedef enum CC2650_I3MOTE_UdmaName {
    CC2650_I3MOTE_UDMA0 = 0,

    CC2650_I3MOTE_UDMACOUNT
} CC2650_I3MOTE_UdmaName;

/*!
 *  @def    CC2650_I3MOTE_GPTimerName
 *  @brief  Enum of GPTimer parts
 */
typedef enum CC2650_I3MOTE_GPTimerName
{
    CC2650_I3MOTE_GPTIMER0A = 0,
    CC2650_I3MOTE_GPTIMER0B,
    CC2650_I3MOTE_GPTIMER1A,
    CC2650_I3MOTE_GPTIMER1B,
    CC2650_I3MOTE_GPTIMER2A,
    CC2650_I3MOTE_GPTIMER2B,
    CC2650_I3MOTE_GPTIMER3A,
    CC2650_I3MOTE_GPTIMER3B,
    CC2650_I3MOTE_GPTIMERPARTSCOUNT
} CC2650_I3MOTE_GPTimerName;

/*!
 *  @def    CC2650_I3MOTE_GPTimers
 *  @brief  Enum of GPTimers
 */
typedef enum CC2650_I3MOTE_GPTimers
{
    CC2650_I3MOTE_GPTIMER0 = 0,
    CC2650_I3MOTE_GPTIMER1,
    CC2650_I3MOTE_GPTIMER2,
    CC2650_I3MOTE_GPTIMER3,
    CC2650_I3MOTE_GPTIMERCOUNT
} CC2650_I3MOTE_GPTimers;

/*!
 *  @def    CC2650_I3MOTE_PWM
 *  @brief  Enum of PWM outputs on the board
 */
typedef enum CC2650_I3MOTE_PWM
{
    CC2650_I3MOTE_PWM0 = 0,
    CC2650_I3MOTE_PWM1,
    CC2650_I3MOTE_PWM2,
    CC2650_I3MOTE_PWM3,
    CC2650_I3MOTE_PWM4,
    CC2650_I3MOTE_PWM5,
    CC2650_I3MOTE_PWM6,
    CC2650_I3MOTE_PWM7,
    CC2650_I3MOTE_PWMCOUNT
} CC2650_I3MOTE_PWM;

/*!
 *  @def    CC2650_I3MOTE_ADCBufName
 *  @brief  Enum of ADCs
 */
typedef enum CC2650_I3MOTE_ADCBufName {
    CC2650_I3MOTE_ADCBuf0 = 0,
    CC2650_I3MOTE_ADCBufCOUNT
} CC2650_I3MOTE_ADCBufName;


/*!
 *  @def    CC2650_I3MOTE_ADCName
 *  @brief  Enum of ADCs
 */
typedef enum CC2650_I3MOTE_ADCName {
    CC2650_I3MOTE_ADC0 = 0,
    CC2650_I3MOTE_ADC1,
    CC2650_I3MOTE_ADC2,
    CC2650_I3MOTE_ADC3,
    CC2650_I3MOTE_ADC4,
    CC2650_I3MOTE_ADC5,
    CC2650_I3MOTE_ADC6,
    CC2650_I3MOTE_ADC7,
    CC2650_I3MOTE_ADCDCOUPL,
    CC2650_I3MOTE_ADCVSS,
    CC2650_I3MOTE_ADCVDDS,
    CC2650_I3MOTE_ADCCOUNT
} CC2650_I3MOTE_ADCName;


#ifdef __cplusplus
}
#endif

#endif /* __CC2650_I3MOTE_BOARD_H__ */
