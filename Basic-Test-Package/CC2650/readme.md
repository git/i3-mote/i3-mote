Basic Test Package for I3mote-CC2650
====================================

Test_CC2650_Blink/
-------------------------
Blinking LEDs basic example.

#### Configuration:
* HeartBeat 0.5s

***

Test CC2650-DebugUART-EchoPC
----------------------------
Echoes back characters received via a PC serial port. 

#### Configuration:
* Uart 115200.
* HeartBeat 0.5s

***

Test MSP432-I2C-Sensors
-----------------------
Reads General Purpose Sensors IDs.

#### Configuration:
* I2C 100kbps.
* Sents debug info through Uart @ 115200b
* Green LED blinks if test result is OK. Green Red on if test fails.

***

Test CC2650-3wSPI-Master-MSP432-SlaveIRQ
----------------------------------------
SPI slave echoes to SPI master using 3-wire mode.
Incrementing data is sent by the master (CC2650) starting at 0x01. 
Received data is expected to be same as the previous transmission.  
Transactions are started from the slave (MSP432) using the IRQ line.

#### Configuration:
* Requires Test MSP432-3wSPI-SlaveIRQ-CC2650-Master running on MSP432
* Sents debug info through Uart @ 115200b


Test CC2650-3wSPI-DAC8730
-------------------------
Reads DAC8730 Unique ID.
Infinite loop of write-read commands at GPVR register.

#### Configuration:
* DAC8730 SPI 10kbps 
* Requires bus connected (24V) to power DAC8730 
* Sents debug info through Uart @ 115200b

***

Tested using *TI-v15.12.1-LTS* Compiler, *TIRTOS 2.20.00.06* and *XDC Tools 3.31.03.43*. 


