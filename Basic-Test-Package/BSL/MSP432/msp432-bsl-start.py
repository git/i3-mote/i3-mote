#!/usr/bin/python

import serial, time


#initialization and open the port

#possible timeout values:
#    1. None: wait forever, block call
#    2. 0: non-blocking mode, return immediately
#    3. x, x is bigger than 0, float allowed, timeout block call
ser = serial.Serial()
ser.port = "/dev/ttyUSB0" # Linux
#ser.port = "COM1" # Windows
ser.baudrate = 115200
ser.bytesize = serial.EIGHTBITS #number of bits per bytes
ser.parity = serial.PARITY_NONE #set parity check: no parity
ser.stopbits = serial.STOPBITS_ONE #number of stop bits
#ser.timeout = None  #block read
#ser.timeout = 1 #non-block read
ser.timeout = 2 #timeout block read
ser.xonxoff = False   #disable software flow control
ser.rtscts = False    #disable hardware (RTS/CTS) flow control
ser.dsrdtr = False    #disable hardware (DSR/DTR) flow control
ser.writeTimeout = 1  #timeout for write
ser.readTimeout = 1  #timeout for read

try: 
    ser.open()
        
except Exception as e:
    print ("error open serial port: " + str(e))
    exit(1)

if ser.isOpen():

    try:
        
        #if inverted:
        set_bootloader_pin = ser.setRTS
        set_reset_pin = ser.setDTR
        
        #else:
        #set_bootloader_pin = ser.setDTR
        #set_reset_pin = ser.setRTS
                
        print("Forze BL[RTS] to Idle State (high)")
        print("Set RST[DTS] active (low) ")

        set_bootloader_pin(0)
        set_reset_pin(1)  
        time.sleep(0.2) 
        
        # Start Sequence: High to Low
        set_bootloader_pin(1)
        time.sleep(0.001) 
        
        # RST Low to High
        set_reset_pin(0) 
        # RST High to Low
        set_reset_pin(1) 
        # RST Low to High Again
        set_reset_pin(0)
        
        # Restore Default State
        time.sleep(0.3)  # Time to enter BSL
        
        ser.close()
        
    except Exception as e1:
        print ("error communicating...: " + str(e1))
        exit(1)

else:
    print ("cannot open serial port ")
    exit(1)
